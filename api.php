<?php

	class WISEANT_API
	{
		function __construct( $id, $key, $login)
		{
			$this->_id = $id;
			$this->_key = $key;
			$this->_owner = $login;

			$this->_init();
		}

		private function _init()
		{
			$this->_data = array();
			$this->_flag = false;
			$this->_authed = false;

			$this->_balance = 0;
			$this->_percent = 0;

			$this->_errorno = 0;

			$this->_socket = fsockopen('151.248.118.242', 3061, $errNo, $errStr, 10);
			$this->_auth();

			$this->_errors = array(
						'Достигнут лимит одновременных соединений, попробуйте позже!',
						'IP с которого сделан запрос не пренадлежит бирже.', 
						'Аккаунт биржи не найден по указанным данным.',
						'Произошла ошибка авторизации! Попробуйте позже.',
						'У нас какие-то проблемы, попробуйте позже.',
						'У нас какие-то проблемы, попробуйте позже.',
						'У нас какие-то проблемы, попробуйте позже.',
						'Недостаточно кредитов на аккаунте переводящего.',
						'Пользователь не найден.',
						'Не найдена открытая заявка пренадлежащая этой бирже.',
						'Код подтверждения неверный.');
		}

		private function _auth()
		{
			$data = array('action' => 'session_up', 'key' => $this->_key, 'id' => $this->_id, 'u' => $this->_owner );
			$this->writeData($data);
			if($this->readData())
			{
				if($this->_data['status'] == true)
				{
					$this->_authed = true;
					$this->_balance = $this->_data['data']['balance'];
					$this->_percent = $this->_data['data']['percent'];
				}
				else
				{
					$this->_closeSocket();
					$this->_authed = false;
				}
			}

		}

		public function reinit()
		{
			$this->_closeSocket();
			$this->_init();
		}

		public function credits_out( $count, $user )
		{
			$data = array('action' => 'credits_out', 'count' => $count, 'user' => $user );
			$this->writeData($data);
			if($this->readData())
			{
				if($this->_data['status'])
				{
					$this->_balance = $this->_data['data']['balance'];
					return true;
				}
				else
					return false;
			}
		}

		public function open_transfer( $count, $user )
		{
			$data = array('action' => 'open_transfer', 'count' => $count, 'user' => $user );
			$this->writeData($data);
			if($this->readData())
			{
				if($this->_data['status'])
					return $this->_data['data']['id'];
				else
					return false;
			}
		}

		public function confirm_transfer( $id, $code )
		{
			$data = array('action' => 'confirm_transfer', 'code' => $code, 'id' => $id );
			$this->writeData($data);
			if($this->readData())
			{
				if($this->_data['status'])
				{
					$this->_balance = $this->_data['data']['balance'];
					return $this->_data['data'];
				}
				else
					return false;
			}
		}

		public function balance()
		{
			return $this->_balance;
		}

		private function closeSocket()
		{
			if(!empty($this->_socket))
			{
				fclose($this->_socket);
				$this->_socket = null;
			}
		}

		private function writeData( $data )
		{
			fputs( $this->_socket, json_encode($data));
		}

		public function error()
		{
			if($this->_errorno == 0) return false;
			return empty($this->_errors[$this->_errorno-1]) ? 'У нас какая-то ошибка! Попробуйте, пожалуйста, позже!' : $this->_errors[$this->_errorno-1];
		}

		private function readData()
		{
			$this->_errorno = 0;
			$data = trim(fgets($this->_socket));

			$this->_data = json_decode($data, true);
			if(empty($this->_data))
			{
				$this->_flag = false;
				$this->_data = array();
			}
			else
			{
				if(!empty($this->_data['errorno']))
					$this->_errorno = $this->_data['errorno'];
				$this->_flag = true;
			}

			return $this->_flag;
		}

		function __destruct()
		{
			$this->closeSocket();
		}
	}
?>